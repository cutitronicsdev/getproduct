import logging
import json
import pymysql
import os

logger = logging.getLogger()
logger.setLevel(logging.INFO)


def openConnection():
    """Generic MySQL DB connection handler"""
    global Connection

    try:
        Connection = pymysql.connect(os.environ['v_db_host'], os.environ['v_username'], os.environ['v_password'], os.environ['v_database'], connect_timeout=5)
    except Exception as e:
        logger.error(e)
        logger.error("ERROR: Unexpected error: Could not connect to MySql instance.")
        raise e


def fn_checkBrandExist(lv_CutitronicsBrandID=None, lv_BrandName=None):

    openConnection()

    lv_list = ""
    lv_statement = "SELECT CutitronicsBrandID, BrandName, ContactName, ContactTitle, ContactEmail FROM BrandDetails WHERE CutitronicsBrandID = %(CutitronicsBrandID)s OR BrandName = %(BrandName)s"
    try:
        with Connection.cursor() as cursor:
            cursor.execute(lv_statement, {'CutitronicsBrandID': lv_CutitronicsBrandID, 'BrandName': lv_BrandName})
            Connection.close()
            for row in cursor:
                field_names = [i[0] for i in cursor.description]
                lv_list = dict(zip(field_names, row))

            if len(lv_list) == 0:
                return(500)
            else:
                return lv_list

    except pymysql.err.IntegrityError as e:
        logging.info(e.args)
        return 500


def fn_checkProductExists(lv_CutitronicsBrandID, lv_CutitronicsSKUCode):

    openConnection()

    lv_list = ""
    lv_statement = "SELECT CutitronicsBrandID, CutitronicsSKUCode, CutitronicsSKUCat, CutitronicsSKUType, CutitronicsSKUName, CutitronicsSKUDesc, DefaultURL, DefaultIMG, DefaultVideo, DefaultVolume, DefaultParameters, RecommendedAmount FROM BrandProducts WHERE CutitronicsBrandID = %(CutitronicsBrandID)s AND CutitronicsSKUCode = %(CutitronicsSKUCode)s"

    try:
        with Connection.cursor() as cursor:
            cursor.execute(lv_statement, {'CutitronicsBrandID': lv_CutitronicsBrandID, 'CutitronicsSKUCode': lv_CutitronicsSKUCode})
            Connection.close()
            for row in cursor:
                field_names = [i[0] for i in cursor.description]
                lv_list = dict(zip(field_names, row))
            
            if len(lv_list) == 0:
                return 400
            else:
                return lv_list
    except pymysql.err.IntegrityError as e:
        logger.error(e.args)
        return 500


def lambda_handler(event, context):
    """
    getBrandProducts

    """
    logger.info("Lambda function - {function_name}".format(function_name=context.function_name))

    # Variables

    lv_msgVersion = None
    lv_testFlag = None
    lv_CutitronicsBrandID = None

    logger.info(' ')
    logger.info("Payload event - '{event}'".format(event=event))
    logger.info(' ')

    lv_CutitronicsBrandID = event.get('multiValueQueryStringParameters')
    lv_CutitronicsSKUCode = lv_CutitronicsBrandID.get('CutitronicsSKUCode')[0]
    lv_CutitronicsBrandID = lv_CutitronicsBrandID.get('CutitronicsBrandID')[0]

    # Return block

    getBrandProducts_out = {"Service": context.function_name, "Status": "Success", "ErrorCode": "", "ErrorDesc": "", "ErrorStack": "", "ServiceOutput": {}}

    try:

        '''
        1. Does brand exist ?
        '''

        logger.info(' ')
        logging.info("Calling fn_checkBrandExist with the following values CutitronicsBrandID - {CutitronicsBrandID}".format(CutitronicsBrandID=lv_CutitronicsBrandID))
        logger.info(' ')

        lv_BrandType = fn_checkBrandExist(lv_CutitronicsBrandID, lv_CutitronicsBrandID)

        if lv_BrandType == 500:  # Failure

            logger.error("Brand '{lv_CutitronicsBrandID}' has not been found in the back office systems".format(lv_CutitronicsBrandID=lv_CutitronicsBrandID))

            getBrandProducts_out['Status'] = "Error"
            getBrandProducts_out['ErrorCode'] = context.function_name + "_001"  # getBrandProducts_out_001
            getBrandProducts_out['ErrorDesc'] = "Supplied brandID does not exist in the BO Database"

            # Lambda response

            return {
                "isBase64Encoded": "false",
                "statusCode": 400,
                "headers": {"x-custom-header": context.function_name, 'Access-Control-Allow-Origin': '*'},
                "body": json.dumps(getBrandProducts_out)
            }

        '''
        2. Does Product exist
        '''

        getBrandProducts_out['ServiceOutput']['BrandProduct'] = {}

        logger.info(' ')
        logging.info("Calling fn_ReturnAllProducts with the following values CutitronicsBrandID - {CutitronicsBrandID}".format(CutitronicsBrandID=lv_CutitronicsBrandID))
        logger.info(' ')

        lv_Product = fn_checkProductExists(lv_CutitronicsBrandID, lv_CutitronicsSKUCode)

        if lv_Product == 400:

            logger.error("")
            logger.error("Product '{lv_SKUCode}' does not exist in the back office.".format(lv_SKUCode=lv_CutitronicsSKUCode))
            logger.error("")

            getBrandProducts_out['Status'] = "Error"
            getBrandProducts_out['ErrorCode'] = context.function_name + "_001"  # getBrandProducts_out_001
            getBrandProducts_out['ErrorDesc'] = "Supplied sku does not exist in the BO Database"

            logger.error("Error completing execution, return: '{lv_ProdOut}'".format(lv_ProdOut=json.dumps(getBrandProducts_out)))
            logger.error("")

            # Lambda response

            return {
                "isBase64Encoded": "false",
                "statusCode": 400,
                "headers": {"x-custom-header": context.function_name, 'Access-Control-Allow-Origin': '*'},
                "body": json.dumps(getBrandProducts_out)
            }

        logger.info("")
        logger.info("Found product '{lv_Prod}' in the back office".format(lv_Prod=lv_CutitronicsSKUCode))
        logger.info("")

        '''
            3. Build Output
        '''
        getBrandProducts_out['ServiceOutput']['CutitronicsBrandID'] = lv_CutitronicsBrandID

        try:

            getBrandProducts_out['ServiceOutput']['BrandProduct']['CutitronicsSKUName'] = lv_Product.get('CutitronicsSKUName')
            getBrandProducts_out['ServiceOutput']['BrandProduct']['CutitronicsSKUDesc'] = lv_Product.get('CutitronicsSKUDesc')
            getBrandProducts_out['ServiceOutput']['BrandProduct']['CutitronicsSKUCode'] = lv_Product.get('CutitronicsSKUCode')
            getBrandProducts_out['ServiceOutput']['BrandProduct']['CutitronicsSKUCat'] = lv_Product.get('CutitronicsSKUCat')
            getBrandProducts_out['ServiceOutput']['BrandProduct']['CutitronicsSKUType'] = lv_Product.get('CutitronicsSKUType')
            getBrandProducts_out['ServiceOutput']['BrandProduct']['DefaultURL'] = lv_Product.get('DefaultURL')
            getBrandProducts_out['ServiceOutput']['BrandProduct']['DefaultIMG'] = lv_Product.get('DefaultIMG')
            getBrandProducts_out['ServiceOutput']['BrandProduct']['DefaultVideo'] = lv_Product.get('DefaultVideo')
            getBrandProducts_out['ServiceOutput']['BrandProduct']['DefaultParameters'] = lv_Product.get('DefaultParameters')
            getBrandProducts_out['ServiceOutput']['BrandProduct']['RecommendedAmount'] = lv_Product.get('RecommendedAmount')

        except Exception:

            logger.error("")
            logger.error("Error getting details for product '{lv_SKUCode}' from the back office.".format(lv_SKUCode=lv_CutitronicsSKUCode))
            logger.error("")

            getBrandProducts_out['Status'] = "Error"
            getBrandProducts_out['ErrorCode'] = context.function_name + "_001"  # getBrandProducts_out_001
            getBrandProducts_out['ErrorDesc'] = "Error getting product from the BO"

            logger.error("Error completing execution, return: '{lv_ProdOut}'".format(lv_ProdOut=json.dumps(getBrandProducts_out)))
            logger.error("")

            # Lambda response

            return {
                "isBase64Encoded": "false",
                "statusCode": 500,
                "headers": {"x-custom-header": context.function_name, 'Access-Control-Allow-Origin': '*'},
                "body": json.dumps(getBrandProducts_out)
            }

        logging.info(' ')
        logging.info("Success: returning the following details to caller - {lv_BrandProducts}".format(lv_BrandProducts=json.dumps(getBrandProducts_out)))
        logging.info(' ')

        return {
            "isBase64Encoded": "false",
            "statusCode": 200,
            "headers": {"x-custom-header": context.function_name, 'Access-Control-Allow-Origin': '*'},
            "body": json.dumps(getBrandProducts_out)
        }

    except Exception as e:
        logger.error("CATCH-ALL ERROR: Unexpected error: '{error}'".format(error=e))

        # Populate Cutitronics reply block

        getBrandProducts_out['Status'] = "Error"
        getBrandProducts_out['ErrorCode'] = context.function_name + "_000"  # regUser_000
        getBrandProducts_out['ErrorDesc'] = "CatchALL"

        # Lambda response

        return {
            "isBase64Encoded": "false",
            "statusCode": 500,
            "headers": {"x-custom-header": context.function_name, 'Access-Control-Allow-Origin': '*'},
            "body": json.dumps(getBrandProducts_out)
        }
